[![pipeline status](https://gitlab.com/omsai/strom-plewis/badges/doxygen/pipeline.svg)](https://gitlab.com/omsai/strom-plewis/pipelines)

# strom
Repository for C++ phylogenetics tutorial

| Resource | Website |
|----------|---------|
| Tutorial | https://phylogeny.uconn.edu/tutorial-v2/             |
| Doxygen  | https://omsai.gitlab.io/strom-plewis/                |
| Coverage | https://omsai.gitlab.io/strom-plewis/coveragereport/ |
